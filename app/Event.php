<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model{
    protected $table = 'events';
    public $_primarykey = 'id';
    public $_idconf='idconf';
    public $_eventtype='eventtype';
    public $_full_name='full_name';
    public $_shortname= 'short_name';
    public $_field='field';
    public $_desc='desc';
    public $_location='location';
    public $_timestart='timestart';
    public $_timeend='timeend';
    public $_alphanumeric_prefix='alphanumeric_prefix';
    public $_active='active';
    public $_createdby='createdby';
    public $_datecreated='datecreated';
    public $timestamps = false;



}