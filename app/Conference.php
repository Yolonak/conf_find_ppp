<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conference extends Model
{
    protected $table = 'conferences';
    public $_primarykey = 'id';
    public $_conftype = 'conftype';
    public $_fullname = 'full_name';
    public $_shortname= 'short_name';
    public $_abbreviaton='abbreviation';
    public $_field='field';
    public $_desc='desc';
    public $_adress='adress';
    public $_country='country';
    public $_url='url';
    public $_contractemail='contact_email';
    public $_datestart='datestart';
    public $_dateend='dateend';
    public $_active='active';
    public $_createdby='createdby';
    public $_datecreated='datecreated';
    public $_logo='logo';
    public $timestamps = false;
    
}
