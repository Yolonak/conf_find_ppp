<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conference;
use App\Event;
use App\Sponsor;

class PagesController extends Controller
{
    public function conferenceHome($conf_id){
        $title='HOME';
        $conf_details=Conference::where('id',$conf_id)->first();
        return view('conferencePages.conferenceHome')->with('conf_details',$conf_details);
    }
    public function conferenceAbout($conf_id){
        $title='ABOUT';
        $conf_details=Conference::where('id',$conf_id)->first();
        return view('conferencePages.conferenceAbout')->with('conf_details',$conf_details);
    }
    public function conferenceSponsors($conf_id){
        $title='SPONSORS';
        $conf_details=Conference::where('id',$conf_id)->first();
        $sponsors=Sponsor::where('idconf',$conf_id)->take(1000)->get();
        return view('conferencePages.conferenceSponsors')->with('conf_details',$conf_details)->with('sponsors',$sponsors);
    }
    public function conferenceSchedule($conf_id){
        $title='SCHEDULE';
        $conf_details=Conference::where('id',$conf_id)->first();
        $event_list=Event::where('idconf',$conf_id)->orderBy('timestart','asc')->take(1000)->get();
        return view('conferencePages.conferenceSchedule')->with('conf_details',$conf_details)->with('event_list',$event_list);
    }
    public function conferenceSearch($conf_id){
        $title='SEARCH';
        $conf_details=Conference::where('id',$conf_id)->first();
        return view('conferencePages.conferenceSearch')->with('conf_details',$conf_details);
    }
    public function conferencesFilter($filter){
        $conferences = Conference::all();
        return view('conferencePages.index')->with('conferences',$conferences)->with('filter',$filter);
    }
}
