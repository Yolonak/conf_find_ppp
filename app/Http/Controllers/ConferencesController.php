<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conference;
use App\Country;
use Carbon\Carbon;

class ConferencesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return Conference::all();
        $conferences = Conference::all();
        //return $conferences;
        return view('conferencePages.index')->with('conferences',$conferences)->with('filter',"none");
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::pluck('name','code');
        return view('conferencePages.create',compact('code','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'full_name'=>'required' ,
            'field'=>'required' ,
            'abbreviation'=>'required' ,
            'adress'=>'required' ,
            'country'=>'required' ,
            'contact_email'=>'required' ,
            'datestart'=>'required' ,
            'dateend'=>'required' ,
            'active'=>'required' ,
            'createdby'=>'required' ,
            'logo' => 'image|nullable|max:1999'

        ]);

        if($request->hasfile('logo')){
            $fileNameWithExt=$request->file('logo')->getClientOriginalName();
            $filename = pathinfo($fileNameWithExt , PATHINFO_FILENAME);
            $fileextension=$request->file('logo')->getClientOriginalExtension();
            $fileNameToStore=$filename .'_'.time().'.'.$fileextension;
            $path=$request->file('logo')->storeAs('public/logos',$fileNameToStore);
        }
        else{
            $fileNameToStore="none.jpg";
        }

        $conference = new Conference;
        $conference->conftype= $request->input('conftype');
        $conference->full_name= $request->input('full_name');
        $conference->short_name= $request->input('short_name');
        $conference->abbreviation= $request->input('abbreviation');
        $conference->field= $request->input('field');
        $conference->desc= $request->input('desc');
        $conference->adress= $request->input('adress');
        $conference->country= $request->input('country');
        $conference->url= $request->input('url');
        $conference->contact_email= $request->input('contact_email');
        $conference->datestart= $request->input('datestart');
        $conference->dateend= $request->input('dateend');
        $conference->active= $request->input('active');
        $conference->createdby= $request->input('createdby');
        $conference->datecreated= Carbon::now()->format('Y-m-d');
        $conference->logo=$fileNameToStore;
        
        //$request->input('datecreated');
        $conference->save();

        return redirect('/conferences');




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
