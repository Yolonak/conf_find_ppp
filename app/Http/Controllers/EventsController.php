<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Carbon\Carbon;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $events=Event::orderBy('id','asc')->paginate(100);
        return view('conferencePages.eventPages.index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('conferencePages.eventPages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'full_name'=>'required' ,
            'field'=>'required' ,
            'idconf'=>'required' ,
            'location'=>'required' ,
            'timestart'=>'required' ,
            'timeend'=>'required' ,
            'alphanumeric_prefix' =>'required',
            'eventtype' =>'required',
            'active'=>'required' ,
            'createdby'=>'required'

        ]);

        $event = new Event;
        $event->idconf= $request->input('idconf');
        $event->full_name= $request->input('full_name');
        $event->eventtype= $request->input('eventtype');
        $event->short_name= $request->input('short_name');
        $event->field= $request->input('field');
        $event->desc= $request->input('desc');
        $event->location= $request->input('location');
        $event->timestart= $request->input('timestart');
        $event->timeend= $request->input('timeend');
        $event->alphanumeric_prefix= $request->input('alphanumeric_prefix');
        $event->active= $request->input('active');
        $event->createdby= $request->input('createdby');
        $event->datecreated= Carbon::now()->format('Y-m-d');
        $event->save();

        return redirect('/events');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event=Event::find($id);
        return view('conferencePages.eventPages.show')->with('event',$event);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        return view('conferencePages.eventPages.edit',compact('event','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'full_name'=>'required' ,
            'field'=>'required' ,
            'idconf'=>'required' ,
            'location'=>'required' ,
            'timestart'=>'required' ,
            'timeend'=>'required' ,
            'alphanumeric_prefix' =>'required',
            'eventtype' =>'required',
            'active'=>'required' ,
            'createdby'=>'required'

        ]);

        $event = Event::find($id);
        $event->idconf= $request->input('idconf');
        $event->full_name= $request->input('full_name');
        $event->eventtype= $request->input('eventtype');
        $event->short_name= $request->input('short_name');
        $event->field= $request->input('field');
        $event->desc= $request->input('desc');
        $event->location= $request->input('location');
        $event->timestart= $request->input('timestart');
        $event->timeend= $request->input('timeend');
        $event->alphanumeric_prefix= $request->input('alphanumeric_prefix');
        $event->active= $request->input('active');
        $event->createdby= $request->input('createdby');
        $event->datecreated= Carbon::now()->format('Y-m-d');
        $event->save();

        return redirect('/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event=Event::find($id);
        $event->delete();
        return redirect('/events');
    }
}
