<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model{
    protected $table = 'sponsors';
    public $_primarykey = 'id';
    public $_idconf='idconf';
    public $_name='name';
    //public $_logo='logo';

    public $timestamps = false;
}