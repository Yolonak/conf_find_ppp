<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model{
    protected $table = 'countries';
    public $_primarykey = 'code';
    public $_name = 'name';
}