<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/conferenceHome/{conf_id}','PagesController@conferenceHome');
Route::get('/conferenceAbout/{conf_id}','PagesController@conferenceAbout');
Route::get('/conferenceSponsors/{conf_id}','PagesController@conferenceSponsors');
Route::get('/conferenceSchedule/{conf_id}','PagesController@conferenceSchedule');
Route::get('/conferenceSearch/{conf_id}','PagesController@conferenceSearch');

Route::resource('conferences','ConferencesController');
Route::resource('events','EventsController');
Route::resource('sponsors','SponsorsController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');