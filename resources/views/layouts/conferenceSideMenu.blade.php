<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<title>{{config('app.name','LSAPP')}}</title>
<style>
body {
    font-family: "Lato", sans-serif;
}

.sidenav {
    display: none;
    height: 100%;
    width: 250px;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    padding-top: 60px;
}

.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
}

.sidenav a:hover {
    color: #f1f1f1;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
</head>
<body>
    @if($conf_details!=null)
        {{$conf_id=$conf_details->id}}
        
    @endif

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="/conferenceHome/{{{$conf_id}}}">Home</a>
  <a href="/conferenceAbout/{{{$conf_id}}}">About</a>
  <a href="/conferenceSponsors/{{{$conf_id}}}">Sponsors</a>
  <a href="/conferenceSchedule/{{{$conf_id}}}">Schedule</a>
</div>

<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>

<script>
function openNav() {
    document.getElementById("mySidenav").style.display = "block";
}

function closeNav() {
    document.getElementById("mySidenav").style.display = "none";
}
</script>
<a href="/conferences" class= "btn btn-default">Go To Conferences</a>
@yield('content')
     
</body>
</html> 