@extends('layouts.conferenceSideMenu')

@section('content')
    <h1>Sponsors</h1>
    @if($conf_details!=null)
        <h3>{{$conf_details->country}}</h3>
    
        <div class="list-group">
                @foreach($sponsors as $sponsor)
                    <h3>{{$sponsor->name}}</h3>
                    <hr>
                @endforeach
        </div>
        
    @else
        <p>Nothing found</p>
    @endif
@endsection