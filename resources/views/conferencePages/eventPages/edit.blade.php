@extends('layouts.headforsome')

<h1>EDIT EVENT</h1>

{!! Form::open(['action'=>['EventsController@update',$event->id],'method'=>"POST"]) !!}

    <div class="form-group">
        {{Form::label('idconf','ID CONFERENCIJE')}}
        {{Form::text('idconf',$event->idconf,['class'=>'form-control','placeholder'=>'ID'])}}
    </div>
    <div class="form-group">
        {{Form::label('full_name','Full Name')}}
        {{Form::text('full_name',$event->full_name,['class'=>'form-control','placeholder'=>'Full Name'])}}
    </div>
    <div class="form-group">
            {{Form::label('short_name','Short Name')}}
            {{Form::text('short_name',$event->short_name,['class'=>'form-control','placeholder'=>'Short Name'])}}
    </div>
    <div class="form-group">
            {{Form::label('desc','Description')}}
            {{Form::textarea('desc',$event->desc,['class'=>'form-control','placeholder'=>'Description'])}}
    </div>
    <div class="form-group">
            {{Form::label('location','Location')}}
            {{Form::text('location',$event->location,['class'=>'form-control','placeholder'=>'Location'])}}
    </div>
    <div class="form-group">
            {{Form::label('timestart','TimeStart')}}
            {{Form::datetime('timestart',$event->timestart,['class'=>'form-control','placeholder'=>'yyyy-mm-dd hh:mm:ss'])}}
    </div>
    <div class="form-group">
            {{Form::label('timeend','TimeEnd')}}
            {{Form::datetime('timeend',$event->timeend,['class'=>'form-control','placeholder'=>'yyyy-mm-dd hh:mm:ss'])}}
    </div>
    <div class="form-group">
            {{Form::label('active','Active')}}
            {{Form::text('active',$event->active,['class'=>'form-control','placeholder'=>'Active'])}}
    </div>
    <div class="form-group">
            {{Form::label('createdby','CreatedBy')}}
            {{Form::text('createdby',$event->createdby,['class'=>'form-control','placeholder'=>'CreatedBy'])}}
    </div>

    <div class="form-group">
            {{Form::label('eventtype','EventType')}}
            {{Form::select('eventtype', array('Lab' => 'Lab', 'Debate' => 'Debate', 'Sponsored content' => 'Sponsored content', 'Other' => 'Other'))}}
    </div>
    <div class="form-group">
            {{Form::label('field','Field')}}
            {{Form::select('field', array('Other' => 'Other', 'Computer Tech' => 'Computer Tech', 'Future Tech' => 'Future Tech', 'Physics' => 'Physics'))}}
    </div>
    <div class="form-group">
        {{Form::label('alphanumeric_prefix','Alphanumeric Prefix')}}
        {{Form::text('alphanumeric_prefix',$event->alphanumeric_prefix,['class'=>'form-control','placeholder'=>'Alphanumeric Prefix'])}}
</div>
    {{Form::hidden('_method','PUT')}}
    {{Form::submit('Submit',['class'=>'btn btn-primary'])}}

{!! Form::close() !!}