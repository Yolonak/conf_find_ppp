@extends('layouts.headforsome')
<h1>{{$event->full_name}}</h1>
<h2>{{$event->short_name}}</h2>

<div>
    <p>FIELD:{{$event->field}}</p>
    <p>LOCATION:{{$event->location}}</p>
    <p>START:{{$event->timestart}} END:{{$event->timeend}}</p>
    <p>CREATED BY:{{$event->createdby}}</p>
    <hr>
    <body>
        <h3>DESCRIPTION</h3>
        <small>{{$event->field}}</small>
    </body>
    <p>
        <a href="/conferenceSchedule/{{$event->id}}"> Back </a>
    </p>
</div>
