@extends('layouts.headforsome')

<div class="row">
    <div class="col-md-12">
        <br />
        <h3 align="center">Events</h3>
        <br />
        <table class="table table-bordered">
            <tr>
                <th>Event id</th>
                <th>Conference id</th>
                <th>Event full name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>

            @foreach($events as $row)
            <tr>
                <td>{{$row->id}}</td>
                <td>{{$row->idconf}}</td>
                <td>{{$row->full_name}}</td>
            
            <td>
                @if(Auth::user()->name == $row->createdby)
                    <a href="{{action('EventsController@edit',$row['id'])}}">Edit</a></td>
                @endif
                <td>
                    @if(Auth::user()->name == $row->createdby)
                        <form method="post" class="delete_form" action="{{action('EventsController@destroy',$row['id'])}}">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="DELETE"/>
                        <button type="submit" class="btn btn-link" onclick="if (!confirm('Are you sure?')) { return false }"><span>Delete</span></button>
                        </form>
                    @endif
                </td>
            </tr>
            @endforeach

    </div>
    <a href="/conferences" class= "btn btn-default">Go To Conferences</a>
</div>
