@extends('layouts.conferenceSideMenu')

@section('content')
    <h1>Home</h1>
    <form method="get" action="/events/create">
        <button type="submit">Create new event</button>
    </form>
    @if($conf_details!=null)
        <h3>{{$conf_details->full_name}}</h3>
        
    @else
        <p>Nothing found</p>
    @endif
@endsection

