@extends('layouts.headforsome')


<body>
        <div id="app">
                <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                        <div class="container">
                            <a class="navbar-brand" href="{{ url('/') }}">
                                {{ config('app.name', 'ConFinder') }}
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                    
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <!-- Left Side Of Navbar -->
                                <ul class="navbar-nav mr-auto">
                    
                                </ul>
                    
                                <!-- Right Side Of Navbar -->
                                <ul class="navbar-nav ml-auto">
                                    <!-- Authentication Links -->
                                    @guest
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                        @if (Route::has('register'))
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                            </li>
                                        @endif
                                    @else
                                        <li class="nav-item dropdown">
                                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>
                    
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>
                    
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </li>
                                    @endguest
                                </ul>
                            </div>
                        </div>
                    </nav>
            <main class="py-4">
                @yield('content')
            </main>
        </div> 
<h1>CONFERENCES</h1>

<div class="btn-group">
        <button><a href="/conferences?filter=none"> Id </a></button>
        <button><a href="/conferences?filter=full_name">Name</a></button>
        <button><a href="/conferences?filter=datestart">Date</a></button>
        <button><a href="/conferences?filter=field">Field</a></button>
</div>
<hr>
<a href="/events"> View events </a>
<form method="get" action="/conferences/create">
    <button type="submit">Create new conference</button>
</form>
<form class="example" action="/conferences">
    <input type="text" placeholder="Search.." name="search">
    <button type="submit"><i class="fa fa-search"></i></button>
</form>

<?php 
if(isset($_GET['filter'])){
	$filter = $_GET['filter'];	
}
if(isset($_GET['search'])){
	$search = $_GET['search'];	
}
?>

@if(count($conferences)>0 && $filter=="none")
    <?php  $conferences=$conferences->sortBy('id')  ?>
@elseif(count($conferences)>0 && $filter=="full_name")
    <?php  $conferences=$conferences->sortBy('full_name')  ?>
@elseif(count($conferences)>0 && $filter=="datestart")
    <?php  $conferences=$conferences->sortBy('datestart')  ?>
@elseif(count($conferences)>0 && $filter=="field")
    <?php  $conferences=$conferences->sortBy('field')  ?>
@endif


@if(count($conferences)>0)
<div id="conferenceList" class="list-group">
    @foreach($conferences as $conference)
        <?php
            if (isset($_GET['search'])){
                $confDetails=$conference->full_name ." ". $conference->short_name ." ". $conference->adress ." ". $conference->country ." ". $conference->field . " ".$conference->datestart;
                //echo $confDetails;
                if(strpos($confDetails,$search)!==false){
                    ?>  id:{{$conf_id=$conference->id}} date-start:{{$conference->datestart}} imagename:{{$conference->logo}}
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <img style="width:66%" src="/storage/logos/{{$conference->logo}}">
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <a href="/conferenceHome/{{{$conf_id}}}" class="list-group-item" style="padding:4pt"  >
                                <h3>{{$conference->full_name}}</h3></a>
                            </div>
                        </div>
                    <?php
                }
            }
            else{
                ?>  id:{{$conf_id=$conference->id}} date-start:{{$conference->datestart}} imagename:{{$conference->logo}}
                    <div class="row">
                        <div class="col-md-2 col-sm-2">
                            <img style="width:66%" src="/storage/logos/{{$conference->logo}}">
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <a href="/conferenceHome/{{{$conf_id}}}" class="list-group-item" style="padding:4pt"  >
                            <h3>{{$conference->full_name}}</h3></a>
                        </div>
                    </div>
                <?php
            }
        
        ?>
    @endforeach
    <!--  $conferences->links() for paginating !-->
    
</div>
@else
    <p>No conferences found</p>
@endif
</body>
</html>