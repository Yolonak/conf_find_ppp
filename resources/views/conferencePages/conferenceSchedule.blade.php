@extends('layouts.conferenceSideMenu')

@section('content')
    <h1>Schedule</h1>
    @if($conf_details!=null)
        <h3>START DATE:</h3>
        <h3>{{$conf_details->datestart}}</h3>
        <h3>END DATE:</h3>
        <h3>{{$conf_details->dateend}}</h3>
        <div class="list-group">
            @foreach($event_list as $event)
        <h3><a href="/events/{{$event->id}}">{{$event->full_name}} </a></h3>
                    <?php $nowTime = new DateTime();
                    $startTime=new DateTime($event->timestart);
                    $endTime=new DateTime($event->timeend);
                    if ($nowTime>$startTime && $nowTime<$endTime){
                        ?><h5 style="color:blue;">  STARTS: {{$event->timestart}}  ENDS:  {{$event->timeend}}</h5>; <?php
                    }
                    else{
                        ?><h5>  STARTS: {{$event->timestart}}  ENDS:  {{$event->timeend}}</h5><?php
                    }
                    ?>
                    <hr>
                    
            @endforeach
        </div>
    @else
        <p>Nothing found</p>
    @endif
@endsection