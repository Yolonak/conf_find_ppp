@extends('layouts.headforsome')

<div id="app">
                <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                        <div class="container">
                            <a class="navbar-brand" href="{{ url('/') }}">
                                {{ config('app.name', 'ConFinder') }}
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                    
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <!-- Left Side Of Navbar -->
                                <ul class="navbar-nav mr-auto">
                    
                                </ul>
                    
                                <!-- Right Side Of Navbar -->
                                <ul class="navbar-nav ml-auto">
                                    <!-- Authentication Links -->
                                    @guest
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                        @if (Route::has('register'))
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                            </li>
                                        @endif
                                    @else
                                        <li class="nav-item dropdown">
                                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>
                    
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>
                    
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </li>
                                    @endguest
                                </ul>
                            </div>
                        </div>
                    </nav>
            <main class="py-4">
                @yield('content')
            </main>
        </div>

<h1>CREATE CONFERENCE</h1>

{!! Form::open(['action'=>'ConferencesController@store','method'=>"POST" , 'enctype'=>'multipart/form-data']) !!}

    <div class="form-group">
        {{Form::label('full_name','Full Name')}}
        {{Form::text('full_name','',['class'=>'form-control','placeholder'=>'Full Name'])}}
    </div>
    <div class="form-group">
            {{Form::label('short_name','Short Name')}}
            {{Form::text('short_name','',['class'=>'form-control','placeholder'=>'Short Name'])}}
    </div>
    <div class="form-group">
            {{Form::label('abbreviation','Abbrevation')}}
            {{Form::text('abbreviation','',['class'=>'form-control','placeholder'=>'Abbrevation'])}}
    </div>
    <div class="form-group">
            {{Form::label('desc','Description')}}
            {{Form::textarea('desc','',['class'=>'form-control','placeholder'=>'Description'])}}
    </div>
    <div class="form-group">
            {{Form::label('adress','Adress')}}
            {{Form::text('adress','',['class'=>'form-control','placeholder'=>'Adress'])}}
    </div>
    <div class="form-group">
            {{Form::label('url','URL')}}
            {{Form::text('url','',['class'=>'form-control','placeholder'=>'URL'])}}
    </div>
    <div class="form-group">
            {{Form::label('contact_email','Email')}}
            {{Form::email('contact_email','',['class'=>'form-control','placeholder'=>'Email'])}}
    </div>
    <div class="form-group">
            {{Form::label('datestart','Datestart')}}
            {{Form::date('datestart','',['class'=>'form-control','placeholder'=>'DateStart'])}}
    </div>
    <div class="form-group">
            {{Form::label('dateend','DateEnd')}}
            {{Form::date('dateend','',['class'=>'form-control','placeholder'=>'DateEnd'])}}
    </div>
    <div class="form-group">
            {{Form::label('active','Active')}}
            {{Form::text('active','',['class'=>'form-control','placeholder'=>'Active'])}}
    </div>
    <div class="form-group">
            {{Form::label('createdby','CreatedBy')}}
            {{Form::text('createdby','',['class'=>'form-control','placeholder'=>'CreatedBy'])}}
    </div>
    <div class="form-group">
            {{Form::label('conftype','ConfType')}}
            {{Form::select('conftype', array('Other' => 'Other', 'Conference' => 'Conference', 'Future Tech' => 'Future Tech', 'Physics' => 'Physics'))}}
    </div>
    <div class="form-group">
            {{Form::label('field','Field')}}
            {{Form::select('field', array('Other' => 'Other', 'Computer Tech' => 'Computer Tech', 'Future Tech' => 'Future Tech', 'Physics' => 'Physics'))}}
    </div>
    <div class="form-group">
            {{Form::label('country','Country')}}
            {{Form::select('country', $countries, null, ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
            {{Form::label('logo','Logo')}}
            {{Form::file('logo')}}
    </div>

    {{Form::submit('Submit',['class'=>'btn btn-primary'])}}

{!! Form::close() !!}