@extends('layouts.conferenceSideMenu')

@section('content')
    <h1>About</h1>
    @if($conf_details!=null)
        <h3>Conference id:{{$conf_details->id}}</h3>
        <h3>Type:{{$conf_details->conftype}}</h3>
        <h3>Field:{{$conf_details->field}}</h3>
        <h3>Short name:{{$conf_details->short_name}}</h3>
        <h3>Abbreviation:{{$conf_details->abbreviation}}</h3>
        <h3>Adress:{{$conf_details->adress}}</h3>
        <h3>Country:{{$conf_details->country}}</h3>
        <h3>Url:{{$conf_details->url}}</h3>
        <h3>Contact email:{{$conf_details->contact_email}}</h3>
        <h3>Created by:{{$conf_details->createdby}}</h3>
        
    @else
        <p>Nothing found</p>
    @endif
@endsection